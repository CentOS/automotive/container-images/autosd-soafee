# AutoSD SOAFEE Image

This image contains a specific setup to run an AutoSD environment that is compliant
with the SOAFEE architecture specification.

## Building

You can build this image by running:

```
podman build --cap-add=sys_admin -t localhost/autosd-soafee:latest .
```

The `--cap-add=sys_admin` argument is required because the image pulls some
container images during the building process.

## Usage

You can pull the image from quay.io (in case you didn't build it):

```sh
podman pull quay.io/centos-sig-automotive/autosd-soafee:latest
```

Next step is to run it with privileged mode:

```sh
podman run --name autosd-soafee-dev -d --privileged quay.io/centos-sig-automotive/autosd-soafee:latest
```

You can now "enter" into your container and play with it:

```sh
podman exec -it autosd-soafee-dev /bin/bash
```

### Devcontainer

You can also use devcontainers by using the following sample file:

```json
{
    "image": "quay.io/centos-sig-automotive/autosd-soafee:latest",
    "privileged": true
}

```

## License

[MIT](./LICENSE)
